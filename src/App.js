import React, { Component } from 'react'
import { connect } from 'react-redux'
import './App.css'
import { fetchBreeds, fetchBreedImage, selectBreed } from './ducks/breeds'
import { BreedImage, Intro, Navbar } from './components'

export class App extends Component {
  // Lifecycle methods

  componentDidMount () {
    const { selectedBreed, fetchBreedImage, fetchBreeds } = this.props
    if (selectedBreed) {
      fetchBreedImage(selectedBreed)
    }
    fetchBreeds()
  }

  shouldComponentUpdate (nextProps) {
    const { fetchBreedImage, selectedBreed } = this.props
    if (selectedBreed !== nextProps.selectedBreed) {
      fetchBreedImage(nextProps.selectedBreed)
      return false
    }
    return true
  }

  // render

  render () {
    if (process.env.NODE_ENV !== 'production') {
      console.log('App render', this.props)
    }
    const {
      breedImage,
      breeds,
      selectedBreed,
      selectBreed
    } = this.props
    return (
      <div className='App'>
        <Navbar
          breeds={breeds}
          selectBreed={selectBreed}
          selectedBreed={selectedBreed}
        />
        <Intro />
        <BreedImage src={breedImage} />
      </div>
    )
  }
}

const mapStateToProps = state => state

const mapDispatchToProps = dispatch => ({
  fetchBreeds: () => dispatch(fetchBreeds()),
  fetchBreedImage: (breed) => dispatch(fetchBreedImage(breed)),
  selectBreed: (e, obj) => dispatch(selectBreed(obj.value))
})

export default connect(mapStateToProps, mapDispatchToProps)(App)
