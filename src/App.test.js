import React from 'react'
import ReactDOM from 'react-dom'
import { App } from './App'

const PROPS_FIXTURE = {
  breedImage: '',
  breeds: [],
  fetchBreeds: () => ([]),
  fetchBreedImage: () => (''),
  selectedBreed: '',
  selectBreed: () => ('')
}

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<App {...PROPS_FIXTURE} />, div)
  ReactDOM.unmountComponentAtNode(div)
})
