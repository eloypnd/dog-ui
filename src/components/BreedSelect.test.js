import React from 'react'
import renderer from 'react-test-renderer'
import BreedSelect from './BreedSelect'

it('BreedSelect renders correctly loading', () => {
  const tree = renderer
    .create(<BreedSelect breeds={[]} />)
    .toJSON()
  expect(tree).toMatchSnapshot()
})

it('BreedSelect renders correctly selected', () => {
  const FIXTURE = [
    {
      key: 'breed1/subbreed1',
      value: 'breed1/subbreed1',
      text: 'Subbreed1 Breed2'
    },
    {
      key: 'breed2/subbreed2',
      value: 'breed2/subbreed2',
      text: 'Subbreed2 Breed2'
    }
  ]
  const tree = renderer
    .create(
      <BreedSelect
        breeds={FIXTURE}
        selectedBreed='breed2/subbreed2'
      />
    )
    .toJSON()
  expect(tree).toMatchSnapshot()
})
