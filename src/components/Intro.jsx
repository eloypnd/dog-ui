import React from 'react'
import { Header } from 'semantic-ui-react'

const Intro = () => (
  <Header>
    Choose a dog breed from the dropdown on the top right.
  </Header>
)

export default Intro
