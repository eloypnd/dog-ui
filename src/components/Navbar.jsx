import React from 'react'
import {
  Container, Image, Menu
} from 'semantic-ui-react'
import { BreedSelect } from '.'

const fixedMenuStyle = {
  backgroundColor: '#fff',
  border: '1px solid #ddd',
  boxShadow: '0px 3px 5px rgba(0, 0, 0, 0.2)'
}

const Navbar = ({ breeds, selectBreed, selectedBreed }) => (
  <Menu
    borderless
    fixed='top'
    style={fixedMenuStyle}
  >
    <Container text>
      <Menu.Item>
        <Image size='mini' src='https://dog.ceo/img/dog-api-logo.svg' />
      </Menu.Item>
      <Menu.Item header>Dog UI</Menu.Item>
      <Menu.Item position='right'>
        <BreedSelect
          breeds={breeds || []}
          selectBreed={selectBreed}
          selectedBreed={selectedBreed}
        />
      </Menu.Item>
    </Container>
  </Menu>
)

export default Navbar
