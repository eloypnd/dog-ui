/**
 * @module ducks/breeds
 *
 * This module follows
 * {@link https://github.com/erikras/ducks-modular-redux|ducks propsal}
 * to orginise actions, reducers and epics.
 */
import { capitalise, handleError } from '../util'
const fetch = window.fetch
const BASE_PATH = process.env.REACT_APP_BASE_PATH || '/'

// ACTIONS
export const REQUEST_BREEDS = 'dog-ui/breed/REQUEST_BREEDS'
export const RECEIVE_BREEDS = 'dog-ui/breed/RECEIVE_BREEDS'
export const FAILURE_BREEDS = 'dog-ui/breed/FAILURE_BREEDS'
export const SELECT_BREED = 'dog-ui/breed/SELECT_BREED'
export const REQUEST_IMAGE = 'dog-ui/breed/REQUEST_IMAGE'
export const RECEIVE_IMAGE = 'dog-ui/breed/RECEIVE_IMAGE'
export const FAILURE_IMAGE = 'dog-ui/breed/FAILURE_IMAGE'

// REDUCER

/**
 * Object describing the initial state
 * @type {Object}
 */
const initialState = {
  breedImage: '',
  breeds: [],
  selectedBreed: ''
}

/**
 * @function reducer
 * @param {Object} [state=initialState]
 * @param {FSA} action @see {@link https://github.com/acdlite/flux-standard-action|FSA}
 * @return {Object}
 */
export default function reducer (state = initialState, action = {}) {
  switch (action.type) {
    case RECEIVE_BREEDS:
      return Object.assign({}, state, {
        breeds: mapBreedsResponse(action.payload)
      })
    case FAILURE_BREEDS:
      return Object.assign({}, state, {
        error: action.error && action.payload
      })
    case SELECT_BREED:
      return Object.assign({}, state, {
        selectedBreed: action.payload
      })
    case RECEIVE_IMAGE:
      return Object.assign({}, state, {
        breedImage: action.payload.message,
        isLoadingImage: false
      })
    case FAILURE_IMAGE:
      return Object.assign({}, state, {
        error: action.error && action.payload,
        isLoadingImage: false
      })
    default:
      return state
  }
}

// ACTION CREATORS

export const receiveBreeds = (payload) => ({
  type: RECEIVE_BREEDS,
  payload
})

export const failureBreeds = (error) => ({
  type: FAILURE_BREEDS,
  payload: error,
  error: true
})

export const receiveImage = (payload) => ({
  type: RECEIVE_IMAGE,
  payload
})

export const failureImage = (error) => ({
  type: FAILURE_IMAGE,
  payload: error,
  error: true
})

// SIDE-EFFECTS

export const init = () => {
  // side-effect: get data from URL
  const { pathname } = window.location
  return {
    type: SELECT_BREED,
    payload: (pathname !== BASE_PATH)
      ? pathname.slice(BASE_PATH.length)
      : ''
  }
}

// side-effect: HTTP request
export const fetchBreeds = () => {
  return (dispatch) => {
    return fetch('https://dog.ceo/api/breeds/list/all')
      .then(handleError)
      .then(payload => dispatch(receiveBreeds(payload)))
      .catch(error => dispatch(failureBreeds(error)))
  }
}

export const selectBreed = (payload) => {
  // side-effect: modify URL
  window.history.pushState({}, '', `${BASE_PATH}${payload}`)
  return {type: SELECT_BREED, payload}
}

// side-effect: HTTP request
export const fetchBreedImage = (breed) => {
  return (dispatch) => {
    return fetch(`https://dog.ceo/api/breed/${breed}/images/random`)
      .then(handleError)
      .then(payload => dispatch(receiveImage(payload)))
      .catch(error => dispatch(failureImage(error)))
  }
}

// helpers

/**
 * this functions is simply the RECEIVE_BREEDS reducer
 * move it here for readability
 *
 * @function mapBreedsResponse
 * @param  {object} message
 * @return {array}
 */
const mapBreedsResponse = ({message}) => {
  const tmp = []
  for (let breed in message) {
    if (message[breed].length) {
      message[breed].forEach(
        subbreed => tmp.push({
          key: `${breed}/${subbreed}`,
          value: `${breed}/${subbreed}`,
          text: `${capitalise(subbreed)} ${capitalise(breed)}`
        })
      )
    } else {
      tmp.push({
        key: breed,
        value: breed,
        text: capitalise(breed)
      })
    }
  }
  return tmp
}
