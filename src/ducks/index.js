import { default as breedsReducer } from './breeds'

// normally we'll use `combineReducers` here
// as far as we only have one reducer
// and to keep example simple
// we make breedsReducers as rootReducer
export default breedsReducer
