import { capitalise, handleError } from './util'

describe('util', () => {
  it('capitalise', () => {
    expect(
      capitalise('aaa')
    ).toEqual('Aaa')
  })

  it('handleError response ok', () => {
    expect(
      handleError({
        ok: true,
        json: () => ({})
      })
    ).toEqual({})
  })

  it('handleError response NOT ok', () => {
    expect(
      () => handleError({ok: false})
    ).toThrow(/Network Error/)
  })
})
